<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'price',
        'description',
        'image',
        'category_id',
    ];

    protected $appends = ['image_url'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getImageUrlAttribute()
    {
        return \Storage::disk('public')->url($this->image);
    }

    public function scopeFilter($query, $filter)
    {
        return $query
            ->when($id = array_get($filter, 'category_id'), function ($query) use ($id) {
                $ids = Category::where('parent_id', $id)->pluck('id')->push($id);
                return $query->whereIn('category_id', $ids);
            })
            ->when($sort = array_get($filter, 'sort'), function ($query) use ($sort) {
                switch ($sort) {
                    case 'name':
                        return $query->orderBy('name');
                    case 'price':
                        return $query->orderBy('price');
                    default:
                        return $query;
                }
            })
        ;
    }
}
