<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index()
    {
        return Category::with('children')->get();
    }

    public function create(Request $request)
    {
        Category::create($request->all());

        return ['result' => 'success'];
    }

    public function delete(Category $category)
    {
        $category->delete();

        return ['result' => 'success'];
    }
}
