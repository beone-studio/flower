<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index()
    {
        return Product::with('category')->latest()->get();
    }

    public function show(Product $product)
    {
        return $product;
    }

    public function create(Request $request)
    {
        Product::create([
            'name' => $request->input('name'),
            'price' => $request->input('price'),
            'description' => $request->input('description'),
            'category_id' => $request->input('category_id'),
            'image' => $request->image->store('products', 'public'),
        ]);

        return ['result' => 'success'];
    }

    public function update(Product $product, Request $request)
    {
        $data = [
            'name' => $request->input('name'),
            'price' => $request->input('price'),
            'description' => $request->input('description'),
            'category_id' => $request->input('category_id'),
        ];

        if ($request->hasFile('image')) {
            \Storage::disk('public')->delete($product->image);
            $data['image'] = $request->image->store('products', 'public');
        }
        $product->update($data);

        return ['result' => 'success'];
    }

    public function delete(Product $product)
    {
        \Storage::disk('public')->delete($product->image);
        $product->delete();

        return ['result' => 'success'];
    }
}
