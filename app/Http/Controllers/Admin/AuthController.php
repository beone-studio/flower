<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        return view('admin.login', [
            'error' => session('error')
        ]);
    }

    public function doLogin(Request $request)
    {
        $user = User::where('username', $request->input('username'))->first();
        if (!$user || !\Hash::check($request->input('password'), $user->password)) {
            return redirect('/admin/login')->with([
                'error' => true
            ]);
        } else {
            \Auth::login($user);
            return redirect('/admin/products');
        }
    }

    public function logout()
    {
        \Auth::logout();

        return redirect('/admin/login');
    }
}
