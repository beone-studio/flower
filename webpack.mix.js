let mix = require('laravel-mix');

mix.disableNotifications()

if (mix.config.inProduction) {
    mix.version()
}

mix
    .js('resources/assets/admin/js/app.js', 'public/assets/admin/js/app.js')
    .less('resources/assets/admin/less/styles.less', 'public/assets/admin/css/app.css')