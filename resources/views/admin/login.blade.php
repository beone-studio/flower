<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Панель управления</title>
    <link rel="stylesheet" href="{{ mix('/assets/admin/css/app.css') }}">
</head>
<body>
<div class="login-page">
    <div class="panel login-form">
        <div class="panel-body">
            <form method="post">
                <h1>Панель управления</h1>
                {{ csrf_field() }}
                <input type="text" placeholder="Логин" name="username"/>
                <input type="password" placeholder="Пароль" name="password"/>
                <button>Вход</button>
                @if ($error)
                    <div class="message">Неверный логин/пароль</div>
                @endif
            </form>
        </div>
    </div>
</div>
</body>
</html>