<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ЧТУП "Страна цветов плюс"</title>
    <link rel="stylesheet" href="/assets/index/css/main.css">
    <link rel="stylesheet" href="/assets/index/css/fonts.css">
    <link rel="stylesheet" href="/assets/index/css/font-awesome.css">
</head>
<body>

<!— Yandex.Metrika informer —>
<a href="https://metrika.yandex.by/stat/?id=35213465&amp;from=info.."
target="_blank" rel="nofollow"><img style="display: none" src="https://informer.yandex.ru/informer/35213465/3_1_FFFFFFFF_EFE.."
style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" class="ym-advanced-informer" data-cid="35213465" data-lang="ru" /></a>
<!— /Yandex.Metrika informer —>

<!— Yandex.Metrika counter —>
<script type="text/javascript" >
(function (d, w, c) {
(w[c] = w[c] || []).push(function() {
try {
w.yaCounter35213465 = new Ya.Metrika({
id:35213465,
clickmap:true,
trackLinks:true,
accurateTrackBounce:true,
webvisor:true
});
} catch(e) { }
});

var n = d.getElementsByTagName("script")[0],
s = d.createElement("script"),
f = function () { n.parentNode.insertBefore(s, n); };
s.type = "text/javascript";
s.async = true;
s.src = "https://mc.yandex.ru/metrika/watch.js";

if (w.opera == "[object Opera]") {
d.addEventListener("DOMContentLoaded", f, false);
} else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/35213465" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!— /Yandex.Metrika counter —>

<header>
    <h1>Страна цветов <span>+</span></h1>
    <ul>
        <li><a href="/">Главная</a></li>
        <li><a href="/catalog">Каталог</a></li>
        <li><a href="/contacts">Контакты</a></li>
        <li class="price"><a href="/assets/index/price/price.xlsx" download>Скачать прайслист</a><span class="fa fa-chevron-down"></span></li>
    </ul>
</header>
<div id="content">
    @yield('content')
</div>
<script src="/assets/index/js/jquery-3.2.1.min.js"></script>
<script src="/assets/index/js/script.js"></script>
</body>
</html>