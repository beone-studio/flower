@extends('index.layout')

@section('content')
<div class="catalog">
    <img src="/assets/index/images/catalog.jpeg" alt="">
</div>
<section class="container"> 
    <h1>Каталог</h1>  
    <div class="hr"></div>
    <ul class="menu">
        @foreach ($categories as $category)
            <li>
                <a class="{{ $filters['category_id'] == $category->id ? 'active' : '' }}" href="/catalog?category_id={{ $category->id }}">{{ $category->name }}</a>
                <ul>
                    @foreach ($category->children as $subCategory)
                        <li>
                            <a class="{{ $filters['category_id'] == $subCategory->id ? 'active' : '' }}" href="/catalog?category_id={{ $subCategory->id }}">{{ $subCategory->name }}</a>
                        </li>
                    @endforeach
                </ul>
            </li>
        @endforeach
    </ul>    
    <div class="maincontainer">
        @foreach ($products as $product)
            <div class="go-to-inner">
                <div class="imgContainer">
                    <a data-popup-open="popup-{{$product -> id}}" href="#"><img class="main" src="../../../storage/{{$product -> image}}" alt=""></a>
                </div>
                <p>
                    {{ $product -> name }}
                </p>                
                <p>
                    {{ $product -> description }}
                </p>
                <p>
                    {{ $product -> price }} руб.
                </p>
            </div>
            <div class="popup" data-popup="popup-{{$product -> id}}">
                <div class="popup-inner">
                    <img src="../../../storage/{{$product -> image}}" alt="">
                    <a class="popup-close" data-popup-close="popup-{{$product -> id}}" href="#">x</a>
                </div>
            </div>
        @endforeach
        {{$products->render()}}
    </div>
</section>
@endsection