@extends('index.layout')

@section('content')
    <div class="contact">
        <img src="/assets/index/images/contact.jpg" alt="">
    </div>
    <section class="container">
        <h1 id="cont">Контакты</h1>
        <div class="hr"></div>
        <ul class="contacts">
            <li><span class="fa fa-phone"></span>+375 (44) 777-72- 52 (Velcom)</li>
            <li><span class="fa fa-phone"></span>+375 (33) 667-61- 77 (МТС)</li>
            <li><span class="fa fa-phone"></span>+375 (17) 268-13- 95</li>
            <li><span class="fa fa-phone"></span>+375 (17) 268-13- 94</li>
            <li><span class="fa fa-envelope"></span>Minskcvet@yandex.ru</li>
        </ul>
        <p class="time"><span class="fa fa-clock-o"></span>с 8-00 до 17-00 (пн-пт)</p>
        <p class="adress"><span class="fa fa-map-marker"></span> ул. Ф.Скорины, 51б корпус 3, г.Минск, 220141</p>

        <p class="descr">Для удобства езды по навигатору лучше использовать адрес ул. Руссиянова, 52 – он приведет вас к повороту
        на транспортную проходную завода. Проехав пересечение
        ул.Руссиянова - ул.Куприевича сразу повернуть налево в Ближайший съезд к домам и взяться правее, на горку к шлагбауму.
        Въехав на территорию завода необходимо объехать желтое здание по левую руку с торца.</p>
        <div class="more">Показать схему проезда</div>
        <img class="come1" src="/assets/index/images/come-1.jpg" alt="">
        <img class="come2" src="/assets/index/images/come-2.jpg" alt="">
    </section>    
@endsection