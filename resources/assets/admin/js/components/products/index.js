import Create from './create.vue'
import Table from './table.vue'
import Edit from './edit.vue'

export default [
    {
        path: '/products',
        component: Table,
        props: {
            title: 'Товары',
            icon: 'cubes',
        },
    },
    { path: '/products/create', component: Create },
    { path: '/products/:id', component: Edit },
]