import Table from './table.vue'

export default [{
    path: '/categories',
    component: Table,
    props: {
        title: 'Категории',
        icon: 'list',
    }
}]