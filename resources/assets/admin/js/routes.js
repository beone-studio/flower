import categories from './components/categories'
import products from './components/products'

export default [
    ...categories,
    ...products,
]