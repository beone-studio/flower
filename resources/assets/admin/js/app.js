import 'babel-polyfill'
import Vue from 'vue'
import VueRouter from 'vue-router'
import ElementUI from 'element-ui'
import ElementUILocale from 'element-ui/lib/locale/lang/ru-RU'
import App from './components/app.vue'
import axios from 'axios'
import panel from './components/general/panel.vue'

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
let token = document.head.querySelector('meta[name="csrf-token"]');
axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

Vue.use(ElementUI, {
    locale: ElementUILocale
})
Vue.component('panel', panel)
Vue.use(VueRouter)
Vue.prototype.$http = {
    get(url, data) {
        return axios.get(`/admin/api/${url}`, { params: data }).then(response => response.data)
    },

    post(url, data) {
        return axios.post(`/admin/api/${url}`, data).then(response => response.data)
    },

    delete (url) {
        return axios.delete(`/admin/api/${url}`).then(response => response.data)
    },
}

const app = new Vue({
    render: h => h(App),
}).$mount('#app')
