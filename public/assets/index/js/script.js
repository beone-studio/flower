$(".more").click(function () {
	$(".come1").fadeIn(500);
	$(".come2").fadeIn(500);
	setTimeout(function () {
		$('html, body').animate({ scrollTop: $(".come1").offset().top - 120}, 800);
	},300);
});
/*$("#main").click(function () {
	$('html, body').animate({ scrollTop: 0}, 800);
});*/
$(window).on("load", function () {
	$("html,body").css("opacity", "1");
})

$(window).scroll(function(event) {
	var top = $(".hr").offset().top - 85;
	if ($(window).scrollTop() >= top) {
		$(".menu").css("position", "fixed");
		$(".menu").css("top", "175px");
		$(".menu").css("left", "5%");
	}
	if ($(window).scrollTop() < top) {
		$(".menu").css("position", "absolute");
		$(".menu").css("top", "275px");
		$(".menu").css("left", "5%");
	}	
});

$(function() {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
 
        e.preventDefault();
    });
 
    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
 
        e.preventDefault();
    });
});