<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/login', 'AuthController@login');
    Route::post('/login', 'AuthController@doLogin');
    Route::get('/logout', 'AuthController@logout');

    Route::group(['middleware' => 'auth'], function () {
        Route::group(['prefix' => 'api'], function () {
            Route::group(['prefix' => 'categories'], function () {
                Route::get('/', 'CategoryController@index');
                Route::post('/', 'CategoryController@create');
                Route::delete('/{category}', 'CategoryController@delete');
            });

            Route::group(['prefix' => 'sub-categories'], function () {
                Route::get('/', 'SubCategoryController@index');
                Route::post('/', 'SubCategoryController@create');
                Route::delete('/{subCategory}', 'SubCategoryController@delete');
            });

            Route::group(['prefix' => 'products'], function () {
                Route::get('/', 'ProductController@index');
                Route::get('/{product}', 'ProductController@show');
                Route::post('/', 'ProductController@create');
                Route::post('/{product}', 'ProductController@update');
                Route::delete('/{product}', 'ProductController@delete');
            });
        });

        Route::any('{all?}', function () {
            return view('admin.main');
        })->where(['all' => '.*']);
    });
});

Route::group(['namespace' => 'Index'], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/catalog', 'CatalogController@index');
    Route::get('/catalog/{product}', 'CatalogController@show');

    Route::get('/contacts', 'PageController@index');
});
